Summary

This project creates API tests for the Sunset & Sunrise API. It alows the user to connect to the server and send a request in order to receive a response that consist of information of the time of sunset and sunrise. Prameters for longitude and latitude are used to receive accurate information.

Tools for the project
Microsoft Visual Studio Code
Python
PIP - Package installer for Python
Windows terminal
Automation tools - requests 2.24.0, flask 1.1.2

Installation

Go to the Python.org website and locate the "Downloads" option in the menu and click the button with the text "Python 3.9.0" (For Mac OS systems -  Click the selection from the downloads menu 'Mac OS X', and select the link for the latest release for Python 3, then proceed to select the link 'macOS 64-bit installer' located under 'Stable Releases'). Follow the necessary steps for installing Python after downloading is completed.

To test if your Python installation is successful, open your terminal and simply type 'python --version'

To test if PIP is also installed in your system while your terminal is still open, simply type 'pip --version'. Note: the Python package installer is already included in Python. 

Open another windows terminal and run the terminal as administrator. 

Change the directory from the path (C:\windows\Systems32) to the (C:\) starting point, and change the directory to type a path that will point to the section where you save your documents by typing this command:
cd "name of your computer"\''username from your computer"\Documents

Once the path is currently pointed to the area on your operating system, type the command to make a directory
md "The name of your directory"    

Change the directory in order to be inside the directory that you created.
cd "The name of your directory"

Inside of your directory, you're going to setup a virtual environment. This capability allows you to contain the Python interpreter, libraries and scripts to keep everything isolated from other virtual environments when by default any libraries are installed in a system.

In your terminal, type the following command to setup the virtual environment inside your directory
py -m venv "create a name of your environment"

After entering the command, type this execution path that will activate your virtual environment. Note: After you entyer the command you will see the name of your virtual environment next to your command line.
"name of your environment"\Scripts\activate

Once your virtual environment is activated, you could install any files from Python using the PIP python package installer. First, for best practice purposes, you need to create a file that will contain the dependencies to use as tools for automation testing. Type this PIP command that will create a text file to use. Note: Locate the directory you created and you will see a blank text file where you could type all of the dependencies. 
pip freeze > requirements.txt

After locating the blank text file in your directory, type these dependencies in the file that could be used as supporting tools for automation
Flask==1.1.2, requests==2.24.0

Once you've saved the text file, type this PIP command in order to install the dependencies in the virtual environment. Note: After executing the command, the installation process will begin.  
pip install -r requirements.txt

Go to the code.visualstudio.com website and click the "Download for Windows - Stable Build" button to download Visual Studio Code IDE. Follow the necessary steps of the installation process. (For Mac OS - Click the down arrow button to display the collapsed menu that contains other download selections, and click the download symbol under the category 'Stable' for the macOS package)

Setup
Setup - Environment

Once the Visual Sudio Code IDE is completely installed in the system, launch the application on your desktop.

Click "open folder", and select your directory that was previously created to use on the VSCode platform.

After all of the details are displayed, you must select a python interpreter in order to translate all python code from line to line. Go to "View" and select "Command Palette", and you will see a collapsed menu containing the interpreters. Select the interpreter that has the name of your virtual environment that is associated with your directory. Note: There will be a prompt that will remind you that the 'Bandit' pylinter is not installed. Click on the selection that will allow VSCode IDE to start the installation process.

Setup - Cloning

VSCode IDE comes with a source control feature in order to clone your git repository from your GitHub account. Click the source control icon, and there will be a display of these options; "initialize repository" or "publish to GitHub".

To use source control, click "initialize repository", and all of the files that are contained will be in green text with the letter 'U' indicating the file is unstaged. 

To stage the files, click on the source control icon and locate the name of all the files used inside the folder, and click the "+" sign to change the files from unstage to stage files to be used to migrate to the GitHub repository. 


